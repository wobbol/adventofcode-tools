#!/bin/sh
# copyright 2021 wobbol
# Public Domain or CC0 whichever the user prefers

# To get and install your session cookie:
# . Open your browser dev tools
# . Find the dev tools tab that says "storage" or similar
# . Rummage around in there till you find your cookies
# . There is probably only one cookie named session
# . That is the one, thats the cookie you need
# . $ get_input.sh --cookie
#   - paste the long hex part of the cookie when it asks.

# If you need any help, ping @wobbol.

version="0.0.1"
pname="$0"

function print_and_save_cookie() {
	conf="$1"
	printf "Session Cookie: " 1>&2
	read cookie
	printf 'cookie=%s\n' "$cookie" >> "$conf"
	printf "%s" "$cookie"
}

function print_and_save_year() {
	conf="$1"
	printf "Year: " 1>&2
	read year
	printf 'year=%d\n' "$year" >> "$conf"
	printf "%d" "$year"
}

function make_url() {
	year="$1"
	day_number="$2"
	printf "https://adventofcode.com/%d/day/%d/input" "$year" "$day_number"
}

function print_and_save_puzzle_input() {
	cookie="$1"
	year="$2"
	day_number="$3"
	puzzle_input=$(curl -A "AoC Input Getter/$version https://github.com/wobbol/adventofcode-tools" --cookie "session=$cookie" "$(make_url "$year" "$day_number")")
	#puzzle_input=$(curl -s --cookie "session=$cookie" "$(make_url "$year" "$day_number")")
	case "$puzzle_input" in
		*Please\ log\ in*) # No cookie or expired cookie.
			# Solution: Check cookie.
			return 1
			;;
		*Internal\ Server\ Error*) # Invalid cookie.
			# Solution: Check cookie.
			return 1
			;;
		*Not\ Found*) # Invalid url. Checked first.
			# Solution: Check year and date.
			return 2
			;;
		*Please\ don\'t*before\ it\ unlocks*) # Live url, but not unlocked. No cookie check.
			# Solution: Wait for the puzzle to unlock.
			return 3
			;;
	esac
	echo Day "$day_number" puzzle input!
	tee "input_$day_number" <<< "$puzzle_input"
	echo "File written: input_$day_number"
	return 0
}

function find_config() {
	if [ -z "$XDG_CONFIG_HOME" ]
	then
		confdir="$HOME/.config/adventofcode-tools"
	else
		confdir="$XDG_CONFIG_HOME/adventofcode-tools"
	fi
	mkdir -p "$confdir"
	conf="$confdir/config"
	if [ ! -e "$conf" ]
	then
		touch "$conf"
	fi
	printf '%s' "$conf"
}

function print_settings() {
	conf="$1"
	printf 'Config file: %s\n' "$conf"
	grep '^[^#].*[^=]$' "$conf"
}

function print_usage() {
	pname="$1"
	printf 'usage: %s { day_number | -c | -y | -s | -h | --cookie | --year | --settings | --help }\n' "$pname"
}

function print_help() {
	pname="$1"
	conf="$2"
	printf 'Config set to %s\n' "$conf"
	printf '\t--cookie   | -c\tsaves your session cookie to config file\n'
	printf '\t--year     | -y\tsaves your year to config file\n'
	printf '\t--settings | -s\tprint the settings\n'
	printf '\t--help     | -h\tprints this help\n'
	print_usage "$pname"
}

if [ -z "$conf" ]
then
	conf="$(find_config)"
fi
. "$conf"

if [ "$#" -ne 1 ]
then
	print_usage "$pname"
	exit 0
fi

day_number=""
case "$1" in
	--cookie | -c)
		print_and_save_cookie "$conf" > /dev/null
		exit 0
		;;
	--year | -c)
		print_and_save_year "$conf" > /dev/null
		exit 0
		;;
	--help | -h)
		print_help "$pname" "$conf"
		exit 0
		;;
	--settings | -s)
		print_settings "$conf"
		exit 0
		;;
	[0-9][0-9] | [0-9])
		day_number="$1"
		if [ "$day_number" -gt '25' ] || [ "$day_number" -lt '0' ]
		then
			printf 'Day number %s out of range.\n' "$day_number"
			exit 0
		fi
		;;
	*)
		print_usage "$pname"
		exit 1
		;;
esac
if [ -z "$cookie" ]
then
	echo Missing session cookie. Update with:
	printf '%s --cookie\n' "$pname"
fi
if [ -z "$year" ]
then
	echo Missing year. Update with:
	printf '%s --year\n' "$pname"
fi
if [ -z "$cookie" ] || [ -z "$year" ]
then
	exit 0
fi

print_and_save_puzzle_input "$cookie" "$year" "$day_number"
case "$?" in
	1)
		printf 'If visiting %s\n' "$(make_url "$year" "$day_number")"
		echo in your browser gives you puzzle input, update your session cookie with:
		printf '%s --cookie\n' "$pname"
		exit 1
		;;
	2)
		echo There might be something wrong with the submitted url
		echo The first url is what was submitted and the second is
		echo a known good url.
		printf '%s\n' "$(make_url "$year" "$day_number")"
		printf '%s\n' "$(make_url "2020" "1")"
		exit 1
		;;
	3)
		echo That puzzle isn\'t unlocked yet, check the calender countdown.
		exit 1
		;;
esac
